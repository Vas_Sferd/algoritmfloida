#ifndef MATRIX_FUNC
#define MATRIX_FUNC

#define MET double // Matrix element type (Только простые типы)
#define RKEY 0 // Random Key

// #define _USE_2D //if you use matrix structure like two-dimensional array

#ifdef _USE_2D
struct matrix2d_
{
	MET** elem; // Указатель на массив указателей на массив элементов. Сначала, массив - строка указателей на элементы в столбцах
	int size_x; // size of matrix string
	int size_y; // size of matrix columnes
};

#define MATRIX struct matrix2d_*
#define MATRIX_STRUCT_SIZE sizeof(struct matrix2d_)

#define GME(PTR_TO_MATRIX_STRUCTURE, X, Y) (PTR_TO_MATRIX_STRUCTURE->elem[X][Y]) // Обращение к элементу матрицы

#else // Одномерная интерпритация матрицы
struct matrix_in_line_
{
	MET* elem; // Указатель на элемент
	int size_x; // size of matrix string
	int size_y; // size of matrix columnes
};

#define MATRIX struct matrix_in_line_*
#define MATRIX_STRUCT_SIZE sizeof(struct matrix_in_line_)

#define GME(PTR_TO_MATRIX_STRUCTURE, X, Y) (PTR_TO_MATRIX_STRUCTURE->elem[(PTR_TO_MATRIX_STRUCTURE->size_x) * (Y) + (X)])
// ^ Обращение к элементу матрицы

#endif

/*Function Declaration*/
extern MATRIX new_Matrix(int size_x, int size_y);
// extern MATRIX Matrix_copy(MATRIX Matrix);
// extern MATRIX Matrix_cwrv(int size_x, int size_y);
// extern MATRIX Matrix_transpose(MATRIX Matrix);
// extern MATRIX Matrix_summ(MATRIX Matrix_1, MATRIX Matrix_2, char mode);
// extern MATRIX Matrix_multiply_by_matrix(MATRIX Matrix_1, MATRIX Matrix_2);

// extern int Matrix_multiply_by_num(MATRIX Matrix, double n);
// extern int Matrix_value_generate(MATRIX Matrix);
extern int Matrix_print(MATRIX Matrix);


// "function2d.c" - Файл с определениями функций



/*
 * // Простейшая программа, которая создаёт матрицу заданного размера
 * // со случайными размерами  
 *
 * int main(void)
 * {
 *	int x, y;
 *	scanf("%d", &x);
 *	scanf("%d", &y);
 *
 *	MATRIX matrix_n1 = matrix_cwrv(x, y);
 *
 *	matrix_print(matrix_n1);
 *
 *	return 0;
 * }
 *
 */

#endif
