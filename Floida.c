/*
 * Floida.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <malloc.h>
#include <math.h>

#include "function2d.h"

/* Function */
MATRIX new_Matrix(int size_x, int size_y)
{
	MATRIX Matrix = (MATRIX)malloc(MATRIX_STRUCT_SIZE);

#ifdef _USE_2D // Матрица - указатель на массив указателей на столбцы.	

	Matrix->elem = (MET**)malloc(size_x * sizeof(MET*)); //  Создание массива-строки указателей на элементы

	for (int i = 0; i < size_x; ++i)
	{
		Matrix->elem[i] = (MET*)malloc(size_y * sizeof(MET));
	}
	// Выделяем память в столбцах

#else

	Matrix->elem = (MET*)malloc(size_x * size_y * sizeof(MET));
	// Выделяем память сразу для всей матрицы

#endif

	Matrix->size_x = size_x; // Копируем данные о размере в данные структуры
	Matrix->size_y = size_y; // ^

	return Matrix;
}

void Matrix_input(MATRIX Matrix)
{
    for(int i = 0; i < Matrix->size_x; ++i)
    {
        for(int j = 0; j < Matrix->size_y; ++j)
        {
        scanf("%lf", &GME(Matrix, i, j));
        }
    }
}

extern int Matrix_print(MATRIX Matrix) // Печать матрицы в stdout
{
	putchar('\n');

	for (int i = 0; i < Matrix->size_x; ++i)
	{
		for (int j = 0; j < Matrix->size_y; ++j)
		{
			printf("%4.0lf", GME(Matrix, i, j));
		}

	putchar('\n');
	}

	return 0;
}

int min(double a, double b)
{
    if(a < b)
		return a;
    else
		return(b);

}

void Matrix_Floid(MATRIX Weight)
{
	for(int k = 0; k < Weight->size_x; ++k)
		for(int i = 0; i < Weight->size_x; ++i)
			for(int j = 0; j < Weight->size_x; ++j)
				GME(Weight, i, j) = min( GME(Weight, i, j), GME(Weight, i, k) + GME(Weight, k, j) );
}
/**/


int main(void)
{
	printf("Введите число вершин, а затем построчно\nвведите весовую матрицу\n");
	
	int size;
	scanf("%d", &size);
	
	MATRIX Weight = new_Matrix(size, size);
		
	
	Matrix_input(Weight);
	// Matrix_print(Weight);
	// Выводим матрицу : Debug

	Matrix_Floid(Weight);
	
	printf("\nResult:\n");
	
	Matrix_print(Weight);

	return 0;
}
